##Alice & Bob are in a call, Carol listens in.

###QuickStart

Rquirements:

1. A CafeX FAS & FCSDK server
2. To edit the two files (index.html and session.php) to point to the IP of your server.
3. To deploy a webserver (e.g. MAMP / WAMP) to host the files and run the php script.

You can read a more detailed article on this implementation [here](https://support.cafex.com/hc/en-us/articles/201777841-What-is-the--code-I-need-to-make-a-video-call-from-a-browser-) or follow the detailed instructions below:




